//
//  MyFilter.h
//  ToJSON
//
//  Created by Nathan Wehr on 9/4/14.
//  Copyright (c) 2014 Nathan Wehr. All rights reserved.
//

// Sample config
/*
 
 "" {
	read {
		load /usr/local/etc/eloquent/extensions/libFileReader.dylib
	
 
		filter {
			load /usr/local/etc/eloquent/extensions/libToJSON.dylib
 
			pattern "(<one>) (<sub-one>) (<sub-two>)"
 
			keys {
				.one 1
				.sub.one 2
				.sub.two 3
 
			}
 
		}
 
	}
 
 }
 
 
 */

// C++
#include <string>
#include <map>
#include <vector>
#include <regex>
#include <sstream>

// boost
#include <boost/regex.hpp>

// jsoncpp
#include <json/json.h>

// Eloquent
#include <Eloquent/Extensions/Filters/Filter.h>
#include <Eloquent/Extensions/Filters/FilterFactory.h>

namespace Eloquent {
	///////////////////////////////////////////////////////////////////////////////
	// ToJSONFilter : Filter
	///////////////////////////////////////////////////////////////////////////////
	class ToJSONFilter : public Filter {
	public:
		ToJSONFilter( const boost::property_tree::ptree::value_type& i_Config )
		: Filter( i_Config )
		{
			m_Pattern = m_Config.second.get_optional<std::string>( "pattern" );
			
			for( boost::property_tree::ptree::iterator it = m_Config.second.get_child( "keys" ).begin(); it != m_Config.second.get_child( "keys" ).end(); ++it ) {
				m_Keys.insert( std::pair<std::string, unsigned int>( (*it).first, (*it).second.get_value<unsigned int>() ) );
			}
			
		}
		
		virtual ~ToJSONFilter() {}
		
		virtual bool operator<<( std::string& io_Data ) {
			// No pattern given, data is assumed to be in json format already
//			if( !m_Pattern.is_initialized() ) {
//				Json::Value			Root;
//				Json::Reader		Reader;
//				Json::FastWriter	Writer;
//				
//				// If our data isn't already in json format, we need to make it that way
//				if( !Reader.parse( io_Data, Root ) ) {
//					Root["unformatted"] = io_Data;
//					io_Data = Writer.write( Root );
//				}
//				
//				return true;
//				
//			}
			
			// See if our data matches the given expression
			try {
				boost::smatch StringMatch;
				
				bool Matched = boost::regex_match( io_Data, StringMatch, boost::regex( m_Pattern.get() ) );
				
				if( Matched ) {
					Json::Value Root;
					
					for( std::map<std::string, unsigned int>::iterator it = m_Keys.begin(); it != m_Keys.end(); ++it ) {
						Json::Path( (*it).first ).make( Root ) = StringMatch[(*it).second].str();
					}
					
					io_Data = Json::FastWriter().write( Root );
					io_Data.pop_back();
					
				}
				
				return Continue( Matched );
				
			} catch( ... ) {
				return false;
			}
			
		}
		
	private:
		boost::optional<std::string> m_Pattern;
		std::map<std::string, unsigned int> m_Keys;
		
	};
	
	///////////////////////////////////////////////////////////////////////////////
	// ToJSONFactory : Filter
	///////////////////////////////////////////////////////////////////////////////
	class ToJSONFactory : public FilterFactory {
	public:
		ToJSONFactory() : FilterFactory() {}
		virtual ~ToJSONFactory() {}
		
		virtual Filter* New( const boost::property_tree::ptree::value_type& i_ConfigNode ) {
			return new ToJSONFilter( i_ConfigNode );
		}
		
	};
	
}

extern "C" void* Attach(void);
extern "C" void* Attach(void) { return new Eloquent::ToJSONFactory(); }

